# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance
from test_station import build_fake_test

def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()
    # call stations_by_distance function
    stations_distance = stations_by_distance(stations, (52.2053, 0.1218))
    build_fake_test
    
    # Printing the closest ten stations and distance
    print ("Closest Ten:")
    print (stations_distance[:10])

    # Print Furthest Ten
    print ('Furthest Ten:')
    print (stations_distance[-10:])


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()

    


