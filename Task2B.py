from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_level_over_threshold

def run():
    #create a list of the stations
    stations = build_station_list()
    #add the latest water level to this list
    update_water_levels(stations)
    #find the list of stations over the treshhold
    return stations_level_over_threshold(stations, 0.8)

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()



