import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_level_with_fit

def run():
    stations = build_station_list()
    update_water_levels(stations)
    highest_stations = stations_highest_rel_level(stations, 5)

    dt = 10

    for s in highest_stations:
        dates, levels = fetch_measure_levels(measure_id = s[2], dt=datetime.timedelta(days=dt))
        # print(dates, levels)
        plot_water_level_with_fit(s[0], dates, levels, 4)

if __name__ == "__main__":
    run()
