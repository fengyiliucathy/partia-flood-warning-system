import pytest
import floodsystem.flood
from floodsystem.station import MonitoringStation
#from Test_list import Build_fake_list

def test2C():
    #create a station 
    latest_level = 5.0
    s = MonitoringStation(None, None, 'some station', None, (0,10), None, None)
    s.latest_level = latest_level
    t = MonitoringStation(None, None, 'some station', None, (3,5), None, None)
    t.latest_level = latest_level
    u = MonitoringStation(None, None, 'some station', None, None, None, None)
    u.latest_level = latest_level
    fake_list = [s,t,u]
    a = floodsystem.flood.stations_highest_rel_level(fake_list, 1)
    b = floodsystem.flood.stations_highest_rel_level(fake_list, 2)
    c = floodsystem.flood.stations_highest_rel_level(fake_list, 3)

    assert len(a)==1
    assert len(b)==2
    assert len(c)==2
