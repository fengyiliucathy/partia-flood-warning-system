import pytest
import floodsystem.flood
from floodsystem.station import MonitoringStation 
#from Test_list import Build_fake_list

def test2B():
    #create a station 
    latest_level = 5.0
    s = MonitoringStation(None, None, 'some station', None, (0,10), None, None)
    s.latest_level = latest_level
    t = MonitoringStation(None, None, 'some station', None, (3,5), None, None)
    t.latest_level = latest_level
    u = MonitoringStation(None, None, 'some station', None, None, None, None)
    u.latest_level = latest_level
    fake_list = [s,t,u]
    a = floodsystem.flood.stations_level_over_threshold(fake_list, 0.8)
    
    assert type(a)==tuple