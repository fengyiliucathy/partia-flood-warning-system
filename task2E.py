import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels


def run():
    stations = build_station_list()
    update_water_levels(stations)
    highest_stations = stations_highest_rel_level(stations, 5)
    print(highest_stations)



    for s in highest_stations:
            dt = 10
            dates, levels = fetch_measure_levels(measure_id = s[2], dt=datetime.timedelta(days=dt))
          
        # print(dates, levels)
            plot_water_levels(s[0], dates, levels)

if __name__ == "__main__":
    run()
