#create a list of fake stations 
from floodsystem.geo import stations_by_distance
from floodsystem.stationdata import build_station_list
from floodsystem.station import MonitoringStation
from floodsystem.stationdata import update_water_levels

def Build_fake_list():
    stations = [MonitoringStation(
        station_id = 'station_id',
        measure_id = 'measure_id',
        label = 'Addlebrook',
        coord = (53.023, 0.0),
        typical_range = (10.0, 20.0),
        river ='River Nile',
        town = 'Hampstead'),
        MonitoringStation(
        station_id = 'station_id',
        measure_id = 'measure_id',
        label = 'Sidney',
        coord = (54.023, 0.456),
        typical_range = (5.0, 25.0),
        river ='River Cam',
        town = 'Small Heath')]
    #for station in stations:
        #station.append(update_level)
    return stations

print (Build_fake_list())

def stations_with_update_water():
    return update_water_levels(Build_fake_list())


