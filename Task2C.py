from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.station import MonitoringStation
from floodsystem.flood import stations_highest_rel_level

def run():
    #create a list of stations
    stations = build_station_list()
    #add the up to date water levels
    update_water_levels(stations)
    #find the N stations at the highest risk of flooding
    return stations_highest_rel_level(stations, 10)

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()
