from floodsystem.stationdata import build_station_list
from  floodsystem.geo import stations_within_radius
from floodsystem.geo import stations_by_distance
import test_station

def run():
    """requirements for Task 1A"""

    #build list of stations
    stations = build_station_list()

    #print number of stations
    print ("number of stations:{}".format(len(stations)))

    #display data from 3 stations
    #for station in stations:
    #    if station.name in ['Bourton Dickler','Surfleet Sluice','Gaw Bridge']:
    #        print(station)
    returnList = stations_within_radius(stations,(52.2053,0.1218),10)
    print(sorted(returnList))

if __name__ == "__main__":
    print ("*** Tsk 1C: CUED Part 1A flood warning system ***")
    run()


