# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from math import radians, cos, sin, asin, sqrt


def haversine(point1, point2, unit='km'):
    """ Calculate the great-circle distance between two points on the Earth surface.

    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.

    Keyword arguments:
    unit -- a string containing the initials of a unit of measurement (i.e. miles = mi)
            default 'km' (kilometers).

    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))

    :output: Returns the distance between the two points.

    The default returned unit is kilometers. The default unit can be changed by
    setting the unit parameter to a string containing the initials of the desired unit.
    Other available units are miles (mi), nautic miles (nmi), meters (m),
    feets (ft) and inches (in).

    """
    # mean earth radius - https://en.wikipedia.org/wiki/Earth_radius#Mean_radius
    AVG_EARTH_RADIUS_KM = 6371.0088

    # Units values taken from http://www.unitconversion.org/unit_converter/length.html
    conversions = {'km': 1,
                   'm': 1000,
                   'mi': 0.621371192,
                   'nmi': 0.539956803,
                   'ft': 3280.839895013,
                   'in': 39370.078740158}

    # get earth radius in required units
    avg_earth_radius = AVG_EARTH_RADIUS_KM * conversions[unit]

    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2

    return 2 * avg_earth_radius * asin(sqrt(d))

from .utils import sorted_by_key


def stations_by_distance(stations, p):
    stations_distance = []
    # loop through all stations
    for station in stations:
        # calculate distance between station and p
        distance = haversine(p, station.coord) 
        # create a tuple of station and distance and add it to a list 
        stations_distance.append((station.name, distance))
    # sort list and return it 
    return sorted_by_key(stations_distance, 1)


def stations_within_radius(stations, centre, r):
    stationList = stations_by_distance(stations, centre)
    returnList = []
    for i in range(len(stationList)):
        if stationList[i][1] < r:
            returnList.append(stationList[i][0])
    return returnList


def rivers_with_station(stations):
    return {x.river for x in stations}

def stations_by_river(stations):
    station_dic = {}
    for river in rivers_with_station(stations):
        #create the station list to be mapped
        station_list = []
        for station in stations:
            if station.river == river:
                station_list.append(station)
        # assign it to the dictionary
        station_dic[river] = station_list
    return station_dic

def rivers_by_station_number(stations, N):
    # create a list of the number of stations monitoring rivers and a dic of numbers of station mapping to river
    no_station = []
    river_no_station = []
    dic = stations_by_river(stations)
    for river, stations in dic.items():
        no_station.append(len(stations))
    no_station.sort()
    min_station = no_station[-N]
    for river, stations in dic.items():
        if len(stations) >= min_station:
            river_no_station.append((river, len(stations)))
    return sorted_by_key(river_no_station, 1, 1)
