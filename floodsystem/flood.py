from floodsystem.station import inconsistent_typical_range_stations 
from floodsystem.utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    #create an empty list in which to store the data    
    data = []
    #check for any inconsistent data in the stations and remove these from the list
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    for station in stations:
        if station.name in inconsistent_stations:
            stations.remove(station)
        
    for station in stations:
        #iterate through the stations ignoring the stations with no new information 
        if station.relative_water_level() == None:
            pass
        #append the stations where the relative_level is above the tolerance to the empty list
        elif station.relative_water_level() > tol:
            data.append((station.name, station.relative_water_level()))

    #sort the list in reverse order with the highest relative_level first
    data= sorted_by_key(data, 1, reverse=True)
    return print (data)

def stations_highest_rel_level(stations, N):
    #create an empty list in which to store the data
    highest_stations = []
    #check for any inconsistent data in the stations and remove these from the list
    inconsistent_stations = inconsistent_typical_range_stations(stations)
    for station in stations:
        if station.name in inconsistent_stations:
            stations.remove(station)
        
    for station in stations:
        #iterate through the stations ignoring the stations with no new information
        if station.relative_water_level() == None:
            pass
        #append the relative level of the stations to the empty list
        else:
            highest_stations.append((station.name, station.relative_water_level()))

    #sort the list in reverse order with the highest relative_level first
    highest_stations = sorted_by_key(highest_stations, 1, reverse=True)
    #return only the first N items on the list
    return print(highest_stations[0:N])
