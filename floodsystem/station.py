# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""
from floodsystem.utils import sorted_by_key

class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d


    def typical_range_consistent(self):
        return self.typical_range is not None and self.typical_range[0] <= self.typical_range[1]

    def relative_water_level(self):
        #check whether there is available data to be used 
        if self.latest_level == None or self.typical_range == None:
            return None
        else:
            #create the variable relative_level using the equation below
            relative_level = (self.latest_level - self.typical_range[0])/(self.typical_range[1]-self.typical_range[0])
            return relative_level     

    def warning_level(self):
            relative_level = self.relative_water_level()
            if relative_level is not None:
                if relative_level >= 0.9:
                    return "severe"
                if relative_level >= 0.7 and relative_level < 0.9:
                    return "high"    
                if relative_level >= 0.5 and relative_level < 0.7:
                    return "medium"
                if relative_level < 0.5:
                    return "low"

def inconsistent_typical_range_stations(stations):
    inconsistent_station_data = []
    for station in stations:
        if not station.typical_range_consistent():
            inconsistent_station_data.append(station.name)
    return inconsistent_station_data


