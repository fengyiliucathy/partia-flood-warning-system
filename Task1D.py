from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def run():
    # make a list of the sations 
    stations = build_station_list()
    #build a set of all the rivers with monitroing stations 
    rivers = rivers_with_station(stations)
    # print the number of rivers with a monitoring station
    print (len(rivers))
    # sort the rivers and print the first ten
    print (sorted(rivers)[:10])
    
    # rivers to test 
    test_rivers = ['River Aire', 'River Cam', 'River Thames']
    # recall the dictionary of all the stations by river
    river_stations = stations_by_river(stations)
    # access the specific rivers required
    for river in test_rivers:
        print (river + ':')
        #print (station_dic[river])
        try: 
            print (sorted([station.name for station in river_stations[river]]))
        except:
            print ('No station for this river')

    
if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()

